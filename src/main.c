/*
 * main.c
 *
 *  Created on: May 19, 2014
 *      Author: raraujo
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "packet.h"
#include "system_utils.h"
#include "threads.h"
#include "retina_event.h"
#include "npoint_tracker.h"

#define BUFSIZE 		1024
#define COM_BUFSIZE 	128

#define SERIAL_PORT_TOTAL	8

#define IDLE				(0) //Waiting for input
#define RECEIVING_LINE		(1) //Receiving input for a serial port
#define RECEIVING_COMMAND	(2) //Receiving input for a serial port

#define SERVER_COMMAND		'/' //Start of a server command, terminated by newline

static volatile int closeServer = 0; //Outside loop variable
/*
 * Socket file descriptor
 * This is a global variable so that the termination handler can execute the
 * shutdown call on the socket to quickly exit the server.
 */
static volatile int socketFd = -1;

/**
 * Handler of Unix termination signals
 * @param param Unused
 */
void termination_handler(int param) {
	(void) param;
	fprintf(stdout, "Exiting\n");
	closeServer = 1;
	//Shutdown the socket fd so that the main thread can exit cleanly.
	if (socketFd != -1) {
		//We are exiting so we don't care about errors
		shutdown(socketFd, SHUT_RDWR);
	}
}

/**
 * Usage menu for the eDVS data format
 * @param exeName Name of the compiled executable
 */
void printUsageMenu(char *exeName) {
	printf("Usage: %s [0-3]\n", exeName);
	printf(" 0   - 2 bytes per event binary 1yyyyyyy.pxxxxxxx (default)\n");
	printf(" 1   - 4 bytes per event (as above followed by 16bit timestamp 1us res)\n");
	printf(" 2   - 5 bytes per event (as above followed by 24bit timestamp 1us res)\n");
	printf(" 3   - 1..3 bytes timestamp (7bits each), time difference (1us resolution)\n");
}

int main(int argc, char *argv[]) {
	(void) argc;
	(void) argv;
	int state = IDLE;
	int index = 0;
	struct packet * packet;
	pthread_mutex_t socketMutex;
	uint64_t lastTimestamp[7];

	pthread_t npointThread = -1;
	struct NPointArg npointerArg;

	/*threads ids which are used to stop for the threads to finish
	 * before accepting a new connection */
	pthread_t tid[SERIAL_PORT_TOTAL * 2];
	struct readerArg readerArgs[SERIAL_PORT_TOTAL]; //arguments for the reader threads
	struct writerArg writerArgs[SERIAL_PORT_TOTAL]; //arguments for the writer threads

	int connectionFd = 0; //the connection file descriptor
	struct sockaddr_in serv_addr;
	char command[COM_BUFSIZE];
	int commandIndex = 0;
	char receptionBuf[BUFSIZE + 1];
	int nread;
	int eDVSDataFormat = EDVS_DATA_FORMAT_DEFAULT;
	bool socketClosed = false, stopRequested = false;
	if (argc > 1) {
		if (isdigit(argv[1][0])) {
			eDVSDataFormat = argv[1][0] - '0';
			if (eDVSDataFormat < 0 || eDVSDataFormat > EDVS_DATA_FORMAT_BIN_TS7B) {
				printf("Unrecognized data format %d\n", eDVSDataFormat);
				eDVSDataFormat = EDVS_DATA_FORMAT_DEFAULT;
				printUsageMenu(argv[0]);
			}
		} else {
			printf("Unrecognized option\n");
			printUsageMenu(argv[0]);
		}
	}

	switch (eDVSDataFormat) {
	case EDVS_DATA_FORMAT_BIN:
		printf("Using the No timestamp mode\n");
		break;
	case EDVS_DATA_FORMAT_BIN_TS2B:
		printf("Using the 2 byte timestamp mode\n");
		break;
	case EDVS_DATA_FORMAT_BIN_TS3B:
		printf("Using the 3 byte timestamp mode\n");
		break;
	case EDVS_DATA_FORMAT_BIN_TS7B:
		printf("Using the 7 bit timestamp mode\n");
		break;
	default:
		return -1;
	}
	/*This signal is useless
	 * It may occurs when the client disconnects suddenly.*/
	signal(SIGPIPE, SIG_IGN);
	struct sigaction new_action, old_action;

	/* Handle stopping signals */
	new_action.sa_handler = termination_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;

	//Respect the ignore settings that the process may have inherited
	sigaction(SIGINT, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGHUP, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGHUP, &new_action, NULL);
	sigaction(SIGTERM, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGTERM, &new_action, NULL);

	/*
	 * Init the threads arguments
	 * There are two sets of arguments, one for the reader threads and
	 * another for the writers.
	 */
	pthread_mutex_init(&socketMutex, NULL);
	char serialPortName[] = "/dev/ttyUSB0\0";
	for (int i = 0; i < SERIAL_PORT_TOTAL; ++i) {
		/* Inform the thread of their id*/
		readerArgs[i].id = i << 5;
		writerArgs[i].id = i << 5;

		//Data format to be used during parsing
		readerArgs[i].eDVSDataFormat = eDVSDataFormat;
		readerArgs[i].lastTimestamp = lastTimestamp;
		/* The reader threads write to the socket directly
		 * so they share a mutex to protect the critical section*/
		readerArgs[i].socketMutex = &socketMutex;
		serialPortName[11] = '0' + i;
		readerArgs[i].serialPortFd = serPortOpen(serialPortName);
		if (readerArgs[i].serialPortFd == -1) {
			fprintf(stderr, "%s is not responding.\nExitting\n", serialPortName);
			return -1; //Any failure to open the serial port at this stage equals termination.
		}
		writerArgs[i].serialPortFd = readerArgs[i].serialPortFd;
		readerArgs[i].serialError = false;
		writerArgs[i].serialError = false;
		//Common stop requesting flag for all the threads
		readerArgs[i].stopRequested = &stopRequested;
		writerArgs[i].stopRequested = &stopRequested;
		//Each writer gets its own input packet queue that this thread will write to
		writerArgs[i].inputQueue = newPacketQueue();
	}
	npointerArg.running = false;
	npointerArg.stopRequested = false;
	npointerArg.socketMutex = &socketMutex;
	npointerArg.lastTimestamp = lastTimestamp;

	//Socket Creation
	socketFd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketFd < 0) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}
	memset(&serv_addr, 0, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(56000);

	if (bind(socketFd, (struct sockaddr*) &serv_addr, sizeof(serv_addr))) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}

	if (listen(socketFd, 10)) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}

	/* Initialize the packet stack*/
	initPacketPool(SERIAL_PORT_TOTAL * 10);

	/*
	 * This while loops ensure we accept a single client
	 * while handling disconnection smoothly
	 */
	while (!closeServer) {
		//It will block here until there is a connection or a termination signal
		connectionFd = accept(socketFd, (struct sockaddr*) NULL, NULL);
		if (connectionFd == -1) {
			if (!closeServer) {
				//No need to print this error message if caused by the shutdown call.
				fprintf(stderr, "Socket creation failed\n");
			}
			continue; //wait for another connection
		}
		//Prints the IP from the connection
		printIncomingConnection(connectionFd);

		//Reset stop variables
		stopRequested = false;
		socketClosed = false;
		memset(lastTimestamp, 0, sizeof(lastTimestamp));
		//It updates the connection file descriptor and creates/starts the new threds
		for (int i = 0; i < SERIAL_PORT_TOTAL; ++i) {
			readerArgs[i].sockedFd = connectionFd;
			pthread_create(&tid[2 * i], NULL, serialReader, &readerArgs[i]);
			pthread_create(&tid[2 * i + 1], NULL, serialWriter, &writerArgs[i]);
		}
		while (!socketClosed && !closeServer) {
			/*
			 * This call is blocked until 3 situations:
			 * 1) data nread > 0
			 * 2) clean exit nread =0
			 * 3) dirty exit nread =-1
			 */
			nread = read(connectionFd, receptionBuf, BUFSIZE);
			if (nread == -1) {
				if (!closeServer) {
					fprintf(stderr, "Socket read failed\n");
				}
				break;
			} else if (nread > 0) {
				//Parsing data received
				for (int i = 0; i < nread; ++i) {
					if (state == IDLE) {
						//In the IDLE state the first character identifies the serial port from 0 to SERIAL_PORT_TOTAL
						if ((receptionBuf[i] - '0' >= 0) && (receptionBuf[i] - '0' < SERIAL_PORT_TOTAL)) {
							state = RECEIVING_LINE;
							index = 0;
							packet = getPacket(); //get new packet from the pool
							packet->header = (receptionBuf[i] - '0') << 5; //set packet id

						} else if (receptionBuf[i] == SERVER_COMMAND) {
							commandIndex = 0;
							state = RECEIVING_COMMAND;
						}
					} else if (state == RECEIVING_LINE) {
						//the state machine will remain in this state until there is a newline
						if (receptionBuf[i] == '\n' || receptionBuf[i] == '\r') {
							packet->data[index++] = receptionBuf[i];
							packet->header += index - 1;
							pushPacket(writerArgs[getPacketId(packet)].inputQueue, packet);
							state = IDLE;
						} else {
							packet->data[index++] = receptionBuf[i];
							if (index == MAX_PACKET_SIZE) {
								/*if current packet is full, it is pushed to the input queue of the thread
								 * and a new packet is fetched*/
								int id = getPacketId(packet);
								packet->header += index - 1;
								pushPacket(writerArgs[id].inputQueue, packet);
								packet = getPacket();
								packet->header = id << 5;
							}
						}
					} else if (state == RECEIVING_COMMAND) {
						command[commandIndex++] = receptionBuf[i];
						if (receptionBuf[i] == '\n' || receptionBuf[i] == '\r') {
							state = IDLE;
							if (strncmp(command, "!P+", 3) == 0) {
								if (npointerArg.running) {
									printf("NPointer Tracking already running\n");
								} else {
									printf("Starting NPointer Tracking\n");
									npointerArg.stopRequested = false;
									//TODO: Add period selection
									npointerArg.period = DEFAULT_PERIOD;
									npointerArg.sockedFd = connectionFd; //Updating the socket
									pthread_create(&npointThread, NULL, npointTracker, &npointerArg);
								}
							} else if (strncmp(command, "!P-", 3) == 0) {
								if (!npointerArg.running) {
									printf("NPointer Tracking is not running\n");
								} else {
									printf("Stopping NPointer Tracking\n");
									npointerArg.stopRequested = true;
									pthread_join(npointThread, NULL);
								}
							} else {
								command[commandIndex] = '\0';
								printf("Unknown command: %s\n", command);
							}

						}
						if (commandIndex == COM_BUFSIZE) {
							commandIndex = 0;
							fprintf(stderr, "Max command size exceeded. Dropping command.\n");
						}
					} else {
						stopRequested = true; //stop every thread
						fprintf(stderr, "Unknown state, this is BAD.\nExitting\n");
						goto error;
						//hate this but it looks better

					}
				}
			} else {
				/*
				 * When the connection is closed, all worker threads are stopped.
				 * If we detect an error with the serial port, we will try to recover
				 * If it proves to be impossible, we terminate.
				 */
				socketClosed = true;
				stopRequested = true;
				fprintf(stdout, "Socket closed\n");
				for (int i = 0; i < SERIAL_PORT_TOTAL; ++i) {
					pthread_join(tid[2 * i], NULL);
					pthread_join(tid[2 * i + 1], NULL); //wait for the threads to finish
					if (readerArgs[i].serialError || writerArgs[i].serialError) {
						//trying to recover the serial port
						close(readerArgs[i].serialPortFd);
						readerArgs[i].serialPortFd = serPortOpen(serialPortName);
						if (readerArgs[i].serialPortFd == -1) {
							fprintf(stderr, "%s is not responding.\nExitting\n", serialPortName);
							goto error;
							//hate this but it looks better
						}
						//Update the serial port file descriptor
						writerArgs[i].serialPortFd = readerArgs[i].serialPortFd;
					}
				}
				if (npointerArg.running) {
					npointerArg.stopRequested = true;
					pthread_join(npointThread, NULL);
				}

				close(connectionFd);
			}
		}

	}
	error:
	//Clean up time!
	close(connectionFd); //doesn't hurt
	close(socketFd);
	for (int i = 0; i < SERIAL_PORT_TOTAL; ++i) {
		close(readerArgs[i].serialPortFd);
	}

	return 0;
}
