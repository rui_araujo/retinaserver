/*
 * threads.c
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "threads.h"
#include "system_utils.h"
#include "packet.h"
#include "retina_event.h"

#define BUFSIZE 1024

#define PARSING_Y		(0)
#define PARSING_X_P		(1)
#define PARSING_T_1		(2)
#define PARSING_T_2		(3)
#define PARSING_T_3		(4)

/**
 * This function will place an event in the packet and push the packet to the socket
 * if there is no room left for another event.
 * @param packet Current packet being used
 * @param event the event data
 * @param tArg thread argument that contains the data format, the socket mutex and the socket file descriptor
 * @param index Pointer to the current index of used data inside of the packet
 * @return 0 if there are no errors
 */
int pushEventToPacket(struct packet * packet, struct retinaEvent *event, struct readerArg * tArg, int *index) {
	switch (tArg->eDVSDataFormat) {
	case EDVS_DATA_FORMAT_BIN:
		packet->data[(*index)++] = event->y | 0x80;
		packet->data[(*index)++] = event->x | event->polarity;
		break;
	case EDVS_DATA_FORMAT_BIN_TS2B:
		packet->data[(*index)++] = event->y | 0x80;
		packet->data[(*index)++] = event->x | event->polarity;
		packet->data[(*index)++] = (event->timestamp >> 8) & 0xFF;
		packet->data[(*index)++] = event->timestamp & 0xFF;
		break;
	case EDVS_DATA_FORMAT_BIN_TS3B:
		packet->data[(*index)++] = event->y | 0x80;
		packet->data[(*index)++] = event->x | event->polarity;
		packet->data[(*index)++] = (event->timestamp >> 16) & 0xFF;
		packet->data[(*index)++] = (event->timestamp >> 8) & 0xFF;
		packet->data[(*index)++] = event->timestamp & 0xFF;
		break;
	case EDVS_DATA_FORMAT_BIN_TS7B:
		packet->data[(*index)++] = event->y | 0x80;
		packet->data[(*index)++] = event->x | event->polarity;
		if (event->timestamp < 128) {
			packet->data[(*index)++] = (event->timestamp & 0x7F) | 0x80;
		} else if (event->timestamp < 16384) {
			packet->data[(*index)++] = (event->timestamp >> 7) & 0x7F;
			packet->data[(*index)++] = (event->timestamp & 0x7F) | 0x80;
		} else {
			packet->data[(*index)++] = (event->timestamp >> 14) & 0x7F;
			packet->data[(*index)++] = (event->timestamp >> 7) & 0x7F;
			packet->data[(*index)++] = (event->timestamp & 0x7F) | 0x80;
		}
		break;
	default:
		return -1;
	}

	// if the packet is full or there is not anymore room for another event, send the packet
	if (*index == MAX_PACKET_SIZE || (MAX_PACKET_SIZE - *index) <= getMaximumEventSize(tArg->eDVSDataFormat)) {
		packet->header += *index - 1;
		if (writePacket(tArg->sockedFd, tArg->socketMutex, packet)) {
			//Clear the packet anyway
			packet->header = tArg->id;
			*index = 0;
			return -1;
		}
		packet->header = tArg->id;
		*index = 0;
	}
	return 0;
}

#define UINT24_MAX		((uint32_t)UINT16_MAX*UINT8_MAX)

void updateGlobalTimestamp(int eDVSDataFormat, uint64_t * globalTimestamp, uint32_t timestamp, uint32_t * lastTimestamp) {
	if (*lastTimestamp == 0) {
		*lastTimestamp = timestamp;
		return;
	}
	switch (eDVSDataFormat) {
	case EDVS_DATA_FORMAT_BIN_TS2B: {
		if (*lastTimestamp < timestamp) {
			globalTimestamp += timestamp - *lastTimestamp;
		} else { // Overflow
			globalTimestamp += timestamp + ( UINT16_MAX - *lastTimestamp);
		}
		*lastTimestamp = timestamp;
		break;
	}
	case EDVS_DATA_FORMAT_BIN_TS3B: {
		if (*lastTimestamp < timestamp) {
			globalTimestamp += timestamp - *lastTimestamp;
		} else { // Overflow
			globalTimestamp += timestamp + ( UINT24_MAX - *lastTimestamp);
		}
		*lastTimestamp = timestamp;
		break;
	}
	case EDVS_DATA_FORMAT_BIN_TS7B: {
		*globalTimestamp += timestamp;
		break;
	}
	case EDVS_DATA_FORMAT_BIN: //Not applicable
		fprintf(stderr, "Binary mode doesn't have a timestamp\n");
		/* no break */
	default:
		return;
	}
}

void * serialReader(void *arg) {
	struct retinaEvent event;
	uint32_t lastTimestamp = 0;
	char receptionBuf[BUFSIZE + 1];
	int nread, index = 0, currentProcessingStep = PARSING_Y;
	struct readerArg * tArg = arg;
	struct packet * packet = getPacket(); //this packet is only freed at exit
	packet->header = tArg->id;
	memset(&event, 0, sizeof(struct retinaEvent));
	while (!(*tArg->stopRequested)) {
		nread = read(tArg->serialPortFd, receptionBuf, BUFSIZE);
		if (nread == -1) {
			fprintf(stderr, "%3d: Serial read failed\n", getHeaderId(tArg->id));
			tArg->serialError = true;
			*tArg->stopRequested = true;
			break;
		} else if (nread > 0) {
#if DEBUG
			receptionBuf[nread] = '\0'; //safe because the buffer has one extra character
			printf("%s", receptionBuf);
			fflush(stdout);
#endif
			if (tArg->id == 0) {
				for (int i = 0; i < nread; ++i) {
					packet->data[index++] = receptionBuf[i];
					//if a newline if found, the packet is sent immediately
					if (index == MAX_PACKET_SIZE || receptionBuf[i] == '\n') {
						packet->header += index - 1;
						if (writePacket(tArg->sockedFd, tArg->socketMutex, packet)) {
							*tArg->stopRequested = true;
							break;
						}
						packet->header = tArg->id;
						index = 0;
					}
				}
			} else {
				/*
				 * Process the retina events accordingly to the data format
				 */
				for (int i = 0; i < nread; ++i) {
					switch (currentProcessingStep) {
					case PARSING_Y:
						if ((receptionBuf[i] & 0x80) != 0) {
							currentProcessingStep = PARSING_X_P;
							event.y = receptionBuf[i] & ADDR_Y;
						} else {
							fprintf(stderr, "%3d: event hiccup: %c\n", getHeaderId(tArg->id), receptionBuf[i]);
						}
						break;
					case PARSING_X_P:
						event.x = receptionBuf[i] & ADDR_X;
						event.polarity = receptionBuf[i] & ADDR_P;
						if (tArg->eDVSDataFormat == EDVS_DATA_FORMAT_BIN) {
							currentProcessingStep = PARSING_Y;
							if (pushEventToPacket(packet, &event, tArg, &index)) {
								*tArg->stopRequested = true;
								break;
							}
						} else {
							event.timestamp = 0; //Clean the time stamp
							currentProcessingStep = PARSING_T_1;
						}
						break;
					case PARSING_T_1: {
						if (tArg->eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS7B) {
							event.timestamp = receptionBuf[i] & TIMESTAMP_7B;
							if ((receptionBuf[i] & 0x80) != 0) {
								currentProcessingStep = PARSING_Y;
								updateGlobalTimestamp(tArg->eDVSDataFormat, &tArg->lastTimestamp[getHeaderId(tArg->id)-1],
										event.timestamp, &lastTimestamp);
								if (pushEventToPacket(packet, &event, tArg, &index)) {
									*tArg->stopRequested = true;
									break;
								}
							} else {
								currentProcessingStep = PARSING_T_2;
							}
						} else {
							currentProcessingStep = PARSING_T_2;
							event.timestamp = receptionBuf[i];
						}
						break;
					}
					case PARSING_T_2: {
						if (tArg->eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS7B) {
							event.timestamp <<= 7;
							event.timestamp |= receptionBuf[i] & TIMESTAMP_7B;
							if ((receptionBuf[i] & 0x80) != 0) {
								currentProcessingStep = PARSING_Y;
								updateGlobalTimestamp(tArg->eDVSDataFormat, &tArg->lastTimestamp[getHeaderId(tArg->id)-1],
										event.timestamp, &lastTimestamp);
								if (pushEventToPacket(packet, &event, tArg, &index)) {
									*tArg->stopRequested = true;
									break;
								}
							} else {
								currentProcessingStep = PARSING_T_3;
							}
						} else {
							event.timestamp <<= 8;
							event.timestamp |= receptionBuf[i];
							if (tArg->eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS2B) {
								currentProcessingStep = PARSING_Y;
								updateGlobalTimestamp(tArg->eDVSDataFormat, &tArg->lastTimestamp[getHeaderId(tArg->id)-1],
										event.timestamp, &lastTimestamp);
								if (pushEventToPacket(packet, &event, tArg, &index)) {
									*tArg->stopRequested = true;
									break;
								}
							} else {
								currentProcessingStep = PARSING_T_3;
							}
						}
						break;
					}
					case PARSING_T_3: {
						currentProcessingStep = PARSING_Y;
						if (tArg->eDVSDataFormat == EDVS_DATA_FORMAT_BIN_TS7B) {
							event.timestamp <<= 7;
							event.timestamp |= receptionBuf[i] & TIMESTAMP_7B;
							if ((receptionBuf[i] & 0x80) != 0) {
								updateGlobalTimestamp(tArg->eDVSDataFormat, &tArg->lastTimestamp[getHeaderId(tArg->id)-1],
										event.timestamp, &lastTimestamp);
								if (pushEventToPacket(packet, &event, tArg, &index)) {
									*tArg->stopRequested = true;
									break;
								}
							} else {
								fprintf(stderr, "%3d: variable time stamp hiccup\n", getHeaderId(tArg->id));
							}
						} else {
							event.timestamp <<= 8;
							event.timestamp |= receptionBuf[i];
							updateGlobalTimestamp(tArg->eDVSDataFormat, &tArg->lastTimestamp[getHeaderId(tArg->id)-1],
									event.timestamp, &lastTimestamp);
							if (pushEventToPacket(packet, &event, tArg, &index)) {
								*tArg->stopRequested = true;
								break;
							}
						}
						break;
					}
					}
				}
			}
		} else { //timeout on the serial read
			if (index > 0) {
				//send the current packet if there data in it.
				packet->header += index - 1;
				if (writePacket(tArg->sockedFd, tArg->socketMutex, packet)) {
					*tArg->stopRequested = true;
					break;
				}
				packet->header = tArg->id;
				index = 0;
			}
		}
	}
	freePacket(&packet);
	return arg;
}

void * serialWriter(void *arg) {
	struct writerArg * tArg = arg;
	while (!(*tArg->stopRequested)) {
		struct packet * packet = popPacket(tArg->inputQueue);
		if (packet != NULL) {
			if (tArg->id != (packet->header & ID_MASK)) {
				//Bad bug if this happens
				fprintf(stderr, "%3d: Received packet meant to %3d\n", getHeaderId(tArg->id), getPacketId(packet));
			} else {
				//If ids match, simply move the data to the serial port
				if (blockingWrite(tArg->serialPortFd, packet->data, getPacketSize(packet) + 1)) {
					fprintf(stderr, "%3d: Serial write failed\n", getHeaderId(tArg->id));
					freePacket(&packet); //return the packet to the pool
					tArg->serialError = true;
					break;
				}
			}
			freePacket(&packet); //return the packet to the pool
		} else {
			usleep(10);
		}
	}
	return arg;
}

