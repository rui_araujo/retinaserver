/*
 * packet.c
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "packet.h"
#include "system_utils.h"

struct {
	struct packet * head; //pointer to the top packet
	pthread_mutex_t mutex; //mutex to protect access to the stack
} packetStack;

struct packetQueue {
	struct packet * head; //pointer to the front of the queue
	struct packet * tail; //pointer to the end of the queue
	pthread_mutex_t mutex; //mutex to protect access to the queue
};

struct packetQueue * newPacketQueue() {
	struct packetQueue * pQ = calloc(sizeof(struct packetQueue), 1);
	if (pQ == NULL) {
		exit(-2); //if we have no memory we have other problems
	}
	pthread_mutex_init(&pQ->mutex, NULL);
	return pQ;
}

void pushPacket(struct packetQueue * pQ, struct packet * packet) {
	pthread_mutex_lock(&pQ->mutex);
	//if there is no head, the packet will be the head and tail
	if (pQ->head == NULL) {
		pQ->head = packet;
		pQ->tail = packet;
	} else {
		//add to the tail
		pQ->tail->next = packet;
		pQ->tail = packet;
	}
	pthread_mutex_unlock(&pQ->mutex);
}

struct packet * popPacket(struct packetQueue * pQ) {
	struct packet * packet;
	pthread_mutex_lock(&pQ->mutex);
	packet = pQ->head;
	if (pQ->head != NULL) {
		//remove the packet from the queue
		pQ->head = pQ->head->next;
		if (pQ->head == NULL) {
			pQ->tail = NULL;
		}
	}
	pthread_mutex_unlock(&pQ->mutex);
	return packet;
}

void initPacketPool(unsigned int capacity) {
	packetStack.head = NULL;
	pthread_mutex_init(&packetStack.mutex, NULL);
	for (unsigned int i = 0; i < capacity; i++) {
		struct packet * packet = calloc(sizeof(struct packet), 1);
		if (packet == NULL) {
			exit(-2); //if we have no memory we have other problems
		}
		//Adding packet to the stack
		packet->next = packetStack.head;
		packetStack.head = packet;
	}
}

struct packet * getPacket(void) {
	struct packet * packet;
	pthread_mutex_lock(&packetStack.mutex);
	if (packetStack.head == NULL) {
		//Run out of packets
		packet = calloc(sizeof(struct packet), 1);
		if (packet == NULL) {
			exit(-2); //if we have no memory we have other problems
		}
	} else {
		//Popping the packet from the stack
		packet = packetStack.head;
		packetStack.head = packetStack.head->next;
	}
	packet->next = NULL; //making sure the pointer is unusable
	pthread_mutex_unlock(&packetStack.mutex);
	return packet;
}

void freePacket(struct packet ** packet) {
	//Safety checks
	if (packet == NULL) {
		fprintf(stderr, "Trying to free a null packet");
		return;
	}
	if (*packet == NULL) {
		fprintf(stderr, "Trying to free a null packet");
		return;
	}
	pthread_mutex_lock(&packetStack.mutex);
	(*packet)->next = packetStack.head;
	packetStack.head = *packet;
	*packet = NULL;
	pthread_mutex_unlock(&packetStack.mutex);
}

int writePacket(int sockedFd, pthread_mutex_t * socketMutex, struct packet * packet) {
	pthread_mutex_lock(socketMutex);
	if (blockingWrite(sockedFd, &packet->header, 1)) {
		fprintf(stderr, "Socket write failed\n");
		pthread_mutex_unlock(socketMutex);
		return -1;
	}
	if (blockingWrite(sockedFd, packet->data, getPacketSize(packet) + 1)) {
		fprintf(stderr, "Socket write failed\n");
		pthread_mutex_unlock(socketMutex);
		return -1;
	}
	pthread_mutex_unlock(socketMutex);
	return 0;
}

