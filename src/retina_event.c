/*
 * retina_event.c
 *
 *  Created on: May 21, 2014
 *      Author: raraujo
 */

#include "retina_event.h"

int getMaximumEventSize(int eDVSDataFormat) {
	switch (eDVSDataFormat) {
	case EDVS_DATA_FORMAT_BIN:
		return 2;
	case EDVS_DATA_FORMAT_BIN_TS2B:
		return 4;
	case EDVS_DATA_FORMAT_BIN_TS3B:
	case EDVS_DATA_FORMAT_BIN_TS7B:
		return 5;
	default:
		return -1;
	}
}
