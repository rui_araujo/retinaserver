#include "npoint_packet.h"
#include "system_utils.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <math.h>

int np_packet_unpack_mocap_data(npoint_mocap_data_t* p, char* packet) {
	char *ptr = packet;

	//    unsigned char sels[] = {112,216,220};
	//    fprint_np_packet_raw_with_sels(packet,len,sels,3);
	//    printf("\n\n");

	p->ts = us_since_epoch();

	// packet id
	memcpy(&p->id, ptr, 2);
	ptr += 2;

	// packet size
	memcpy(&p->size, ptr, 2);
	ptr += 2;

	if (p->id != NPOINT_PACKET_ID_MOCAP_DATA) {
		return false;
	}

	// frame number
	memcpy(&p->frame_nr, ptr, 4);
	ptr += 4;

	// marker sets
	memcpy(&p->nMarkerSets, ptr, 4);
	ptr += 4;
	p->markerSets = ptr;
	ptr = np_packet_unpack_mocap_data_skip_markerSets(ptr, p->nMarkerSets);

	// other markers
	memcpy(&p->nOtherMarkers, ptr, 4);
	ptr += 4;
	p->otherMarkers = ptr;
	ptr = np_packet_unpack_mocap_data_skip_otherMarkers(ptr, p->nOtherMarkers);

	//printf("ptr at position = %ld\n", ptr-packet);

	// rigid bodies
	memcpy(&p->nRigidBodies, ptr, 4);
	ptr += 4;
	p->rigidBodies = ptr;
	ptr = np_packet_unpack_mocap_data_skip_rigidBodies(ptr, p->nRigidBodies);

	//printf("ptr at position = %ld\n", ptr-packet);

	// skeletons (version 2.1 and later)
	if (((NPOINT_PACKET_VER_MAJOR == 2) && (NPOINT_PACKET_VER_MINOR > 0)) || (NPOINT_PACKET_VER_MAJOR > 2)) {
		memcpy(&p->nSkeletons, ptr, 4);
		fflush(stdout);
		ptr += 4;
		p->skeletons = ptr;
		ptr = np_packet_unpack_mocap_data_skip_skeletons(ptr, p->nSkeletons);
	}

	// labeled markers (version 2.3 and later)
	if (((NPOINT_PACKET_VER_MAJOR == 2) && (NPOINT_PACKET_VER_MINOR >= 3)) || (NPOINT_PACKET_VER_MAJOR > 2)) {
		memcpy(&p->nLabeledMarkers, ptr, 4);
		fflush(stdout);
		ptr += 4;
		p->labeledMarkers = ptr;
		ptr = np_packet_unpack_mocap_data_skip_labeledMarkers(ptr, p->nLabeledMarkers);
	}

	// latency
	memcpy(&p->latency, ptr, 4);
	ptr += 4;

	// timecode
	memcpy(&p->timecode, ptr, 4);
	ptr += 4;

	// timecode sub
	memcpy(&p->timecodeSub, ptr, 4);
	ptr += 4;

	// end of data tag
	memcpy(&p->endOfData, ptr, 4);
	ptr += 4;

	//printf("ptr at position = %ld\n", ptr-packet);
	return true;
}

/*********************************************************************************************

 markerSets

 *********************************************************************************************/

char* np_packet_unpack_mocap_data_skip_markerSets(char* ptr, uint32_t nMarkerSets) {

	//just to update the ptr position
	for (uint32_t i = 0; i < nMarkerSets; i++) {
		ptr = np_packet_unpack_mocap_data_skip_markerSet(ptr);
	}

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_markerSet(char* ptr) {
	uint32_t nMarkers;
	char name[256];
	int name_len = 0;

	strncpy(name, ptr, 256);
	name_len = strlen(name) + 1;
	ptr += name_len;

	memcpy(&nMarkers, ptr, 4);
	ptr += 4;

	return np_packet_unpack_mocap_data_skip_markers(ptr, nMarkers);
}

char* np_packet_unpack_mocap_data_skip_markers(char* ptr, uint32_t nMarkers) {
	for (uint32_t i = 0; i < nMarkers; i++) {
		ptr += 4;   // x
		ptr += 4;   // y
		ptr += 4;   // z
	}

	return ptr;
}

void np_packet_unpack_mocap_data_markerSet(npoint_marker_set_t* ms, npoint_mocap_data_t* md, int index) {
	int i = 0;
	int name_len = 0;
	char* ptr = md->markerSets;

	for (i = 0; i != index; i++) {
		ptr = np_packet_unpack_mocap_data_skip_markerSet(ptr);
	}

	// markerSet at index
	ms->index = index;

	strncpy(ms->name, ptr, 256);
	name_len = strlen(ms->name) + 1;
	ptr += name_len;

	memcpy(&ms->nMarkers, ptr, 4);
	ptr += 4;

	ms->markers = ptr;

	//markers
	//ptr = np_packet_unpack_mocap_data_skip_markers(ptr, ms->nMarkers);
}

void np_packet_unpack_mocap_data_markerSet_marker(vec_3d_t* m, npoint_marker_set_t* ms, int index) {
	np_packet_unpack_mocap_data_marker(m, ms->markers, index);
}

void np_packet_unpack_mocap_data_marker(vec_3d_t* m, char* marker_list, int index) {
	char* ptr = marker_list + index * sizeof(float) * 3;

	// x
	memcpy(&m->x, ptr, 4);
	ptr += 4;

	// y
	memcpy(&m->y, ptr, 4);
	ptr += 4;

	// z
	memcpy(&m->z, ptr, 4);
	ptr += 4;
}

/*********************************************************************************************

 otherMarkers

 *********************************************************************************************/

char* np_packet_unpack_mocap_data_skip_otherMarkers(char* ptr, uint32_t nOtherMarkers) {
	return (ptr + 3 * 4 * nOtherMarkers);
}

void np_packet_unpack_mocap_data_otherMarker(vec_3d_t* m, npoint_mocap_data_t* md, int index) {
	np_packet_unpack_mocap_data_marker(m, md->otherMarkers, index);
}

/*********************************************************************************************

 rigidBodies

 *********************************************************************************************/

char* np_packet_unpack_mocap_data_skip_rigidBodies(char* ptr, uint32_t nRigidBodies) {
	for (uint32_t j = 0; j < nRigidBodies; j++) {
		ptr = np_packet_unpack_mocap_data_skip_rigidBody(ptr);

	} // next rigid body

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_rigidBody(char* ptr) {
	uint32_t nRigidMarkers = 0;

	ptr += 4;       // ID
	ptr += 4;       // x
	ptr += 4;       // y
	ptr += 4;       // z
	ptr += 4;       // qx
	ptr += 4;       // qy
	ptr += 4;       // qz
	ptr += 4;       // qw

	// nRigidMarkers
	memcpy(&nRigidMarkers, ptr, 4);
	ptr += 4;

	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers(ptr, nRigidMarkers);

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_rigidMarkers(char* ptr, uint32_t nRigidMarkers) {
	// marker data
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_markers(ptr, nRigidMarkers);

	if (NPOINT_PACKET_VER_MAJOR >= 2) {
		// associated marker IDs
		ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_marker_ids(ptr, nRigidMarkers);

		// associated marker sizes
		ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_marker_sizes(ptr, nRigidMarkers);
	}

	if (NPOINT_PACKET_VER_MAJOR >= 2) {
		ptr += 4; // Mean marker error
	}

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_rigidMarkers_markers(char* ptr, uint32_t nRigidMarkers) {
	// marker data
	ptr += nRigidMarkers * 3 * sizeof(float);

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_rigidMarkers_marker_ids(char* ptr, uint32_t nRigidMarkers) {
	// marker IDs
	ptr += nRigidMarkers * sizeof(int);

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_rigidMarkers_marker_sizes(char* ptr, uint32_t nRigidMarkers) {
	// marker sizes
	ptr += nRigidMarkers * sizeof(float);

	return ptr;
}

void np_packet_unpack_mocap_data_rigidBody(npoint_rigid_body_t* rb, npoint_mocap_data_t* md, int index) {
	char* ptr = md->rigidBodies;
	int i = 0;

	for (i = 0; i != index; i++) {
		ptr = np_packet_unpack_mocap_data_skip_rigidBody(ptr);
	}

	// rigidBody at index
	rb->index = index;

	// ID
	memcpy(&rb->id, ptr, 4);
	ptr += 4;

	// x
	memcpy(&rb->pos.x, ptr, 4);
	ptr += 4;

	// y
	memcpy(&rb->pos.y, ptr, 4);
	ptr += 4;

	// z
	memcpy(&rb->pos.z, ptr, 4);
	ptr += 4;

	// qx
	memcpy(&rb->q.x, ptr, 4);
	ptr += 4;

	// qy
	memcpy(&rb->q.y, ptr, 4);
	ptr += 4;

	// qz
	memcpy(&rb->q.z, ptr, 4);
	ptr += 4;

	// qw
	memcpy(&rb->q.w, ptr, 4);
	ptr += 4;

	// nRigidMarkers
	memcpy(&rb->nRigidMarkers, ptr, 4);
	ptr += 4;

	rb->rigidMarkers = ptr;

	if (NPOINT_PACKET_VER_MAJOR >= 2) {
		np_packet_unpack_mocap_data_rigidBody_mean_error(&rb->rigidMarkers_mean_error, rb);
	} else {
		rb->rigidMarkers_mean_error = 0.0f;
	}
}

void np_packet_unpack_mocap_data_rigidBody_mean_error(float* mean_error, npoint_rigid_body_t* rb) {
	char* ptr = rb->rigidMarkers;

	// skip markers
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_markers(ptr, rb->nRigidMarkers);

	// skip marker ids
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_marker_ids(ptr, rb->nRigidMarkers);

	// skip marker sizes
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_marker_sizes(ptr, rb->nRigidMarkers);

	// makers mean error
	memcpy(mean_error, ptr, 4);
}

void np_packet_unpack_mocap_data_rigidMarker(npoint_rigid_marker_t* rm, npoint_rigid_body_t* rb, int index) {
	// marker
	np_packet_unpack_mocap_data_rigidMarker_marker(&rm->m, rb, index);

	if (NPOINT_PACKET_VER_MAJOR >= 2) {
		// marker ID
		np_packet_unpack_mocap_data_rigidMarker_marker_id(&rm->id, rb, index);

		// marker size
		np_packet_unpack_mocap_data_rigidMarker_marker_size(&rm->size, rb, index);
	} else {
		rm->id = 0;
		rm->size = 0.0f;
	}

}

void np_packet_unpack_mocap_data_rigidMarker_marker(vec_3d_t* m, npoint_rigid_body_t* rb, int index) {
	np_packet_unpack_mocap_data_marker(m, rb->rigidMarkers, index);
}

void np_packet_unpack_mocap_data_rigidMarker_marker_id(uint32_t* id, npoint_rigid_body_t* rb, int index) {
	int i = 0;
	char* ptr = rb->rigidMarkers;

	// marker data
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_markers(ptr, rb->nRigidMarkers);

	// skip ids
	for (i = 0; i != index; i++) {
		ptr += 4;
	}

	// id at index
	memcpy(id, ptr, 4);
}

void np_packet_unpack_mocap_data_rigidMarker_marker_size(float* size, npoint_rigid_body_t* rb, int index) {
	int i = 0;
	char* ptr = rb->rigidMarkers;

	// skip markers
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_markers(ptr, rb->nRigidMarkers);

	// skip marker ids
	ptr = np_packet_unpack_mocap_data_skip_rigidMarkers_marker_ids(ptr, rb->nRigidMarkers);

	// skip sizes
	for (i = 0; i != index; i++) {
		ptr += 4;
	}

	// id at index
	memcpy(size, ptr, 4);
}

/*********************************************************************************************

 skeletons

 *********************************************************************************************/

char* np_packet_unpack_mocap_data_skip_skeletons(char* ptr, uint32_t nSkeletons) {
	uint32_t nRigidBodies = 0;
	uint32_t nRigidMarkers = 0;

	int nBytes = 0;

	for (uint32_t j = 0; j < nSkeletons; j++) {
		// skeleton id
		ptr += 4;

		// number of rigid bodies
		memcpy(&nRigidBodies, ptr, 4);
		ptr += 4;

		for (uint32_t k = 0; k < nRigidBodies; k++) {
			ptr += 4;       // ID
			ptr += 4;       // x
			ptr += 4;       // y
			ptr += 4;       // z
			ptr += 4;       // qx
			ptr += 4;       // qy
			ptr += 4;       // qz
			ptr += 4;       // qw

			// associated marker positions
			memcpy(&nRigidMarkers, ptr, 4);
			ptr += 4;

			// marker data
			nBytes = nRigidMarkers * 3 * sizeof(float);
			ptr += nBytes;

			// associated marker IDs
			nBytes = nRigidMarkers * sizeof(int);
			ptr += nBytes;

			// associated marker sizes
			nBytes = nRigidMarkers * sizeof(float);
			ptr += nBytes;

			ptr += 4; // Mean marker error

		} // next rigid body

	} // next skeleton

	return ptr;
}

/*********************************************************************************************

 labeledMarkers

 *********************************************************************************************/

char* np_packet_unpack_mocap_data_skip_labeledMarkers(char* ptr, uint32_t nLabeledMarkers) {
	for (uint32_t j = 0; j < nLabeledMarkers; j++) {
		ptr = np_packet_unpack_mocap_data_skip_labeledMarker(ptr);
	}

	return ptr;
}

char* np_packet_unpack_mocap_data_skip_labeledMarker(char* ptr) {
	ptr += 4;   // id
	ptr += 4;   // x
	ptr += 4;   // y
	ptr += 4;   // z
	ptr += 4;   // size

	return ptr;
}

