/*
 * system_utils.c
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */
#include "system_utils.h"
#include <arpa/inet.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>

int serPortOpen(char *serPortName) {
	int serPortFD;
	struct termios tty;

	if ((serPortFD = open(serPortName, O_RDWR)) < 0) {
		printf("can't open serial port file %s\n", serPortName);
		return (-1);
	}

	tcgetattr(serPortFD, &tty); /* get record of parameters */

	//Hard coded to 4M because that's what it is used in all retina and robot
	cfsetospeed(&tty, (speed_t) B4000000); /* output speed */
	cfsetispeed(&tty, (speed_t) B4000000); /* input speed */

	/*
	 * The serial port is configured as non canonical mode, ie, raw mode.
	 * A timeout is set to make sure every packet is sent even if is not full
	 */
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
	tty.c_oflag &= ~OPOST;
	tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
	tty.c_cflag &= ~(CSIZE | PARENB);
	tty.c_cflag |= CS8;/* 8 bits */
	tty.c_cflag |= CLOCAL | CREAD;
	tty.c_cflag |= CRTSCTS;						// use hardware handshaking
	tty.c_cc[VMIN] = 0;
	tty.c_cc[VTIME] = 10;						// 1 second timeout
	/* write modified record of parameters to port */
	if (tcsetattr(serPortFD, TCSANOW, &tty)) {
		printf("serPort fatal: error during configuration!\n");
	}
	//Set the serial port to blocking mode
	if (fcntl(serPortFD, F_SETFL, 0)) {
		close(serPortFD);
		return -1;
	}
	return serPortFD;
}

void printIncomingConnection(int connectionFd) {
	socklen_t len;
	struct sockaddr_storage addr;
	char ipstr[INET6_ADDRSTRLEN];
	getpeername(connectionFd, (struct sockaddr*) &addr, &len);
	struct sockaddr_in *s = (struct sockaddr_in *) &addr;
	inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
	fprintf(stdout, "New connection from %s:%d\n", ipstr, ntohs(s->sin_port));
}

int blockingWrite(int fd, unsigned char * buf, int size) {
	int nwrite = 0;
	for (int i = 0; i < size; i += nwrite) {
		/* loop in time! */
		/* write might not take it all in one call,
		 * so we have to try until it's all written
		 */
		nwrite = write(fd, buf + i, size - i);
		if (nwrite < 0) {
			//Non blocking file descriptor support
			if (errno != EWOULDBLOCK) {
				return -1;
			}
		}
	}
	return 0;
}

uint64_t us_since_epoch() {
	struct timeval tv;
	uint64_t micros = 0;
	gettimeofday(&tv, NULL);
	micros = ((uint64_t) tv.tv_sec) * 1000000 + tv.tv_usec;
	return micros;
}
