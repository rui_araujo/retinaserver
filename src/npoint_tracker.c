/*
 * npoint_tracker.c
 *
 *  Created on: May 26, 2014
 *      Author: raraujo
 */

#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ctype.h>

#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "npoint_tracker.h"
#include "npoint_packet.h"
#include "packet.h"

/*
 * %9.6f format may lead to a 17 character number +1 for the space.
 * 18*6 + 3 = 111 //TODO: update this math
 * The three extra characters are for the null terminator, and the string "-P"
 */
#define MAX_REPORT_LINE								111
#define NP_CONN_MAX_PACKET_LEN              		20000
#define DEFAULT_TRACKER_CONN_DATA_PORT  			56000

struct reportArg {
	pthread_t id;
	bool * stopRequested; //Pointer to the stop requesting flag
	bool updated; //Flag that indicates the reception of data
	pthread_mutex_t locMutex; //shared mutex to protect the location data.
	int sockedFd; //Socket file descriptor
	pthread_mutex_t * socketMutex; //shared mutex to protect the serial port.
	vec_3d_t pos;
	rpy_t eulerAngles;
	int period; //period of reporting in milliseconds
	uint64_t * lastTimestamp;
};

/**
 *
 * @param rpy_deg
 * @param q
 */
void quaternion_to_rpy(rpy_t* rpy_deg, const quaternion_t* q) {
	rpy_deg->roll = atan2(2 * (q->x * q->y + q->w * q->z), q->w * q->w + q->x * q->x - q->y * q->y - q->z * q->z);
	rpy_deg->roll = (rpy_deg->roll * 180) / M_PI;
	rpy_deg->pitch = atan2(2 * (q->y * q->z + q->w * q->x), q->w * q->w - q->x * q->x - q->y * q->y + q->z * q->z);
	rpy_deg->pitch = (rpy_deg->pitch * 180) / M_PI;
	rpy_deg->yaw = asin(-2 * (q->x * q->z - q->w * q->y));
	rpy_deg->yaw = (rpy_deg->yaw * 180) / M_PI;
}

#define round(x) (int)((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

void * npointReporter(void *arg) {
	struct reportArg * reportArg = (struct reportArg *) arg;
	struct packet * packet = getPacket();
	char buf[MAX_REPORT_LINE];
	int counter = reportArg->period;
	uint64_t globalTimestamp = 0;
	while (!(*reportArg->stopRequested)) {
		counter = reportArg->period;
		while (--counter >= 0) {
			usleep(1000);
			if (*reportArg->stopRequested) {
				break;
			}
		}
		if (reportArg->updated) {
			for (int i = 0; i < 7; ++i) {
				if (globalTimestamp < reportArg->lastTimestamp[i]) {
					globalTimestamp = reportArg->lastTimestamp[i];
				}
			}
			pthread_mutex_lock(&reportArg->locMutex);
			reportArg->updated = false;
			reportArg->pos.x = roundf(reportArg->pos.x * 1000);
			reportArg->pos.y = roundf(reportArg->pos.y * 1000);
			reportArg->pos.z = roundf(reportArg->pos.z * 1000);
			reportArg->eulerAngles.yaw = roundf(reportArg->eulerAngles.yaw * 10);
			reportArg->eulerAngles.pitch = roundf(reportArg->eulerAngles.pitch * 10);
			reportArg->eulerAngles.roll = roundf(reportArg->eulerAngles.roll * 10);
			snprintf(buf, MAX_REPORT_LINE, "-P %.0f %.0f %.0f %.0f %.0f %.0f %"PRIu64"\n", reportArg->pos.x,
					reportArg->pos.y, reportArg->pos.z, reportArg->eulerAngles.yaw, reportArg->eulerAngles.pitch,
					reportArg->eulerAngles.roll, globalTimestamp/1000);
			pthread_mutex_unlock(&reportArg->locMutex);
			int length = strlen(buf);
			int index = 0;
			for (int i = 0; i < length; ++i) {
				packet->data[index++] = buf[i];
				if (index == MAX_PACKET_SIZE) {
					packet->header = index - 1;
					if (writePacket(reportArg->sockedFd, reportArg->socketMutex, packet)) {
						*reportArg->stopRequested = true;
						break;
					}
					index = 0;
				}
			}
			if (index > 0) {
				packet->header = index - 1;
				if (writePacket(reportArg->sockedFd, reportArg->socketMutex, packet)) {
					*reportArg->stopRequested = true;
					break;
				}
				index = 0;
			}
		}
	}
	freePacket(&packet);
	return arg;
}

void * npointTracker(void *arg) {
	struct reportArg reportArg;
	char buf[NP_CONN_MAX_PACKET_LEN];
	struct sockaddr_in local_sockaddr;    // structure for server address, port, etc.
	int unicastSD, nread, unexpectedBodiesWarning = false;
	struct NPointArg * tArg = (struct NPointArg*) arg;
	tArg->running = true;
	npoint_mocap_data_t md;
	npoint_rigid_body_t rb;
// get a socket descriptor

	memset(&local_sockaddr, 0, sizeof(local_sockaddr));

	local_sockaddr.sin_family = AF_INET;
	local_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	local_sockaddr.sin_port = htons(DEFAULT_TRACKER_CONN_DATA_PORT);

	if ((unicastSD = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		fprintf(stderr, "NPoint Socket creation failed %d\n", errno);
		tArg->running = false;
		return NULL;
	}

// bind socket to address and port
	if (bind(unicastSD, (struct sockaddr *) &local_sockaddr, sizeof(struct sockaddr))) {
		fprintf(stderr, "NPoint Socket bind failed %d\n", errno);
		tArg->running = false;
		close(unicastSD);
		return NULL;
	}
	//Set Non bocking
	int flags = fcntl(unicastSD, F_GETFL, 0);
	flags |= O_NONBLOCK;
	fcntl(unicastSD, F_SETFL, flags);

	pthread_mutex_init(&reportArg.locMutex, NULL);
	memset(&reportArg.eulerAngles, 0, sizeof(rpy_t));
	memset(&reportArg.pos, 0, sizeof(vec_3d_t));
	reportArg.updated = false;
	reportArg.period = tArg->period;
	reportArg.sockedFd = tArg->sockedFd;
	reportArg.socketMutex = tArg->socketMutex;
	reportArg.stopRequested = &tArg->stopRequested;
	reportArg.lastTimestamp = tArg->lastTimestamp;
	pthread_create(&reportArg.id, NULL, npointReporter, &reportArg);
	while (!tArg->stopRequested) {
		nread = recvfrom(unicastSD, buf, sizeof(buf), MSG_DONTWAIT, NULL, NULL);
		if (nread == -1) {
			if (errno != EWOULDBLOCK) {
				tArg->stopRequested = true;
				fprintf(stderr, "NPoint Socket read failed %d\n", errno);
			} else {
				usleep(1000);
			}
		} else if (nread > 0) {
			if (!np_packet_unpack_mocap_data(&md, buf)) {
				fprintf(stderr, "Failed to read a complete packet\n");
				continue;
			}
			if (md.nRigidBodies > 1 && !unexpectedBodiesWarning) {
				fprintf(stderr, "More than one rigid body received\nReporting the position of the first one\n");
				unexpectedBodiesWarning = true;
			}
			// invoke rigid body handlers
			np_packet_unpack_mocap_data_rigidBody(&rb, &md, 0);

			//Updating last data
			pthread_mutex_lock(&reportArg.locMutex);
			quaternion_to_rpy(&reportArg.eulerAngles, &rb.q);
			memcpy(&reportArg.pos, &rb.pos, sizeof(vec_3d_t));
			reportArg.updated = true;
			pthread_mutex_unlock(&reportArg.locMutex);

		}
	}
	tArg->stopRequested = true;
	pthread_join(reportArg.id, NULL); //waiting for the reporter to finish
	close(unicastSD);
	tArg->running = false;
	return arg;
}
