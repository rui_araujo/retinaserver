#ifndef NPOINT_PACKET_H
#define NPOINT_PACKET_H


#define NPOINT_PACKET_ID_MOCAP_DATA             0x07
#define NPOINT_PACKET_ID_DATA_DESCRIPTIONS      0x05

#define NPOINT_PACKET_VER_MAJOR                     3
#define NPOINT_PACKET_VER_MINOR                     0

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
	float x;
	float y;
	float z;
} vec_3d_t;

typedef struct {
	float x;
	float y;
	float z;
	float w;
} quaternion_t;

typedef struct {
	float roll;
	float pitch;
	float yaw;
} rpy_t;

typedef struct
{
    vec_3d_t m;

    uint32_t id;
    float size;

}npoint_rigid_marker_t;


typedef struct
{
    uint32_t id;
    vec_3d_t m;
    float size;

}npoint_labeled_marker_t;


typedef struct
{
    int index;
    char name[256];

    uint32_t nMarkers;
    char* markers;

} npoint_marker_set_t;


typedef struct {
	int index;
	uint32_t id;

	vec_3d_t pos;
	quaternion_t q;

	uint32_t nRigidMarkers;
	char* rigidMarkers;
	float rigidMarkers_mean_error;

} npoint_rigid_body_t;

typedef struct {
	uint64_t ts;

	uint16_t id;
	uint16_t size;
	uint32_t frame_nr;
	uint32_t nMarkerSets;
	uint32_t nOtherMarkers;
	uint32_t nRigidBodies;
	uint32_t nSkeletons;
	uint32_t nLabeledMarkers;

	char* markerSets;
	char* otherMarkers;
	char* rigidBodies;
	char* skeletons;
	char* labeledMarkers;

	float latency;
	uint32_t timecode;
	uint32_t timecodeSub;
	uint32_t endOfData;

} npoint_mocap_data_t;

int np_packet_unpack_mocap_data(npoint_mocap_data_t* p, char* packet);

// markerSets
char* np_packet_unpack_mocap_data_skip_markerSets(char* ptr, uint32_t nMarkerSets);
char* np_packet_unpack_mocap_data_skip_markerSet(char* ptr);
char* np_packet_unpack_mocap_data_skip_markers(char* ptr, uint32_t nMarkers);

void np_packet_unpack_mocap_data_markerSet(npoint_marker_set_t* ms, npoint_mocap_data_t* md, int index);
void np_packet_unpack_mocap_data_markerSet_marker(vec_3d_t* m, npoint_marker_set_t* ms, int index);
void np_packet_unpack_mocap_data_marker(vec_3d_t* m, char* marker_list, int index);

int np_packet_unpack_mocap_data_find_markerSet(npoint_mocap_data_t* md, const char *name);
int np_packet_unpack_mocap_data_get_markerSet(npoint_marker_set_t* ms, npoint_mocap_data_t* md, const char *name);

// otherMarkers
char* np_packet_unpack_mocap_data_skip_otherMarkers(char* ptr, uint32_t nOtherMarkers);
void np_packet_unpack_mocap_data_otherMarker(vec_3d_t* m, npoint_mocap_data_t* md, int index);

// rigidBodies
char* np_packet_unpack_mocap_data_skip_rigidBodies(char* ptr, uint32_t nRigidBodies);
char* np_packet_unpack_mocap_data_skip_rigidBody(char* ptr);
char* np_packet_unpack_mocap_data_skip_rigidMarkers(char* ptr, uint32_t nRigidMarkers);
char* np_packet_unpack_mocap_data_skip_rigidMarkers_markers(char* ptr, uint32_t nRigidMarkers);
char* np_packet_unpack_mocap_data_skip_rigidMarkers_marker_ids(char* ptr, uint32_t nRigidMarkers);
char* np_packet_unpack_mocap_data_skip_rigidMarkers_marker_sizes(char* ptr, uint32_t nRigidMarkers);

void np_packet_unpack_mocap_data_rigidBody(npoint_rigid_body_t* rb, npoint_mocap_data_t* md, int index);
void np_packet_unpack_mocap_data_rigidBody_mean_error(float* mean_error, npoint_rigid_body_t* rb);

void np_packet_unpack_mocap_data_rigidMarker(npoint_rigid_marker_t* rm, npoint_rigid_body_t* rb, int index);
void np_packet_unpack_mocap_data_rigidMarker_marker(vec_3d_t* m, npoint_rigid_body_t* rb, int index);
void np_packet_unpack_mocap_data_rigidMarker_marker_id(uint32_t* id, npoint_rigid_body_t* rb, int index);
void np_packet_unpack_mocap_data_rigidMarker_marker_size(float* size, npoint_rigid_body_t* rb, int index);

int np_packet_unpack_mocap_data_get_rigidBody(npoint_rigid_body_t* rb, npoint_mocap_data_t* md, const char *name);
void np_packet_unpack_mocap_data_get_rigidBody_name(char* name, npoint_rigid_body_t* rb, npoint_mocap_data_t* md);

// skeletons
char* np_packet_unpack_mocap_data_skip_skeletons(char* ptr, uint32_t nSkeletons);

// labeledMarkers
char* np_packet_unpack_mocap_data_skip_labeledMarkers(char* ptr, uint32_t nLabeledMarkers);
char* np_packet_unpack_mocap_data_skip_labeledMarker(char* ptr);


#ifdef __cplusplus
}
#endif

#endif // NPOINT_PACKET_H
