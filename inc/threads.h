/*
 * threads.h
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */

#ifndef THREADS_H_
#define THREADS_H_

#include <pthread.h>
#include "packet.h"
#include <stdint.h>

struct writerArg {
	unsigned char id;//thread id which is the same as serial port id
	int serialPortFd;//Serial port file descriptor
	bool serialError;//Flag that indicates there was an error with the serial port
	bool * stopRequested;//Pointer to the stop requesting flag
	struct packetQueue * inputQueue;//Packet input queue (main insert push packet through this object)
};


struct readerArg {
	unsigned char id;//thread id which is the same as serial port id
	int eDVSDataFormat;//retina data format
	int serialPortFd;//Serial port file descriptor
	bool serialError;//Flag that indicates there was an error with the serial port
	bool * stopRequested;//Pointer to the stop requesting flag
	int sockedFd;//Socket file descriptor
	uint64_t * lastTimestamp;
	pthread_mutex_t * socketMutex;//shared mutex to protect the serial port.
};

/**
 * Serial Reader thread.
 * For id == 0 it will move all received bytes to the output socket.
 * For other ids it will parse the data and push complete retina events to the packet.
 * @param arg Pointer to the struct readerArg
 */
void * serialReader(void *arg);

/**
 * Serial Writer thread.
 * This thread writes the data contained inside of the received packets to its serial port.
 * The packet id is checked before writing.
 * The input queue is polled at a 100kHz rate.
 * @param arg Pointer to the struct writerArg
 */
void * serialWriter(void *arg);
#endif /* THREADS_H_ */
