/*
 * packet.h
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */

#ifndef PACKET_H_
#define PACKET_H_

#include <pthread.h>

#define MAX_PACKET_SIZE			(32)
#define SIZE_MASK				(MAX_PACKET_SIZE-1)
#define ID_MASK					(0xE0)

struct packet {
	unsigned char header; //encodes the id(3 topmost bits) and the packet size (5 LSB)
	unsigned char data[MAX_PACKET_SIZE]; //Packet payload
	struct packet * next; //pointer which is used by the queue and the pool, not to be used by the user
};

struct packetQueue;
//forward declaration of the packet queue

/**
 * Builds a new packet queue.
 * @return a new packet queue
 */
struct packetQueue * newPacketQueue();

/**
 * Pushes a packet to the queue
 * @param pQ packet queue
 * @param packet the pointer to the packet
 */
void pushPacket(struct packetQueue * pQ, struct packet * packet);

/**
 * Retrieves a packet from the packet queue
 * @param pQ packet queue
 * @return the pointer to the packet or NULL if empty
 */
struct packet * popPacket(struct packetQueue * pQ);

/**
 * Initialize the packet pool.
 * A stack based on a linked list is created at initialization
 * to have packet readily available during execution
 * @param capacity The stack depth
 */
void initPacketPool(unsigned int capacity);

/**
 * A pointer to the packet retrieved from the pool.
 * It must be freed using the freePacket function.
 * This function is thread safe.
 * @return the pointer to the packet
 */
struct packet * getPacket(void);

/**
 * Return the packet to the pool. The packet is set to NULL so it can't be used afterwards.
 * This function is thread safe.
 * @param packet the pointer to the packet, it is a double pointer to be set to NULL.
 */
void freePacket(struct packet ** packet);
/**
 * Macro like function that return the size of the packet.
 * @param packet the pointer to the packet
 * @return the size encoded in the packet header
 */
static inline int getPacketSize(struct packet * packet) {
	return packet->header & SIZE_MASK;
}

/**
 * Macro like function that return the id of the fro.
 * @param packet packet the pointer to the packet
 * @return the id encoded in the packet header
 */
static inline int getHeaderId(unsigned char header) {
	return header >> 5;
}

/**
 * Macro like function that return the id of the packet.
 * @param packet packet the pointer to the packet
 * @return the id encoded in the packet header
 */
static inline int getPacketId(struct packet * packet) {
	return getHeaderId(packet->header);
}

/**
 * Writes a packet to the socket.
 * This function is thread safe.
 *
 * @param sockedFd socket file descriptor
 * @param socketMutex Mutex used to protect the socket access
 * @param packet pointer to the packet
 * @return 0 if there are no errors
 */
int writePacket(int sockedFd, pthread_mutex_t * socketMutex, struct packet * packet);
#endif /* PACKET_H_ */
