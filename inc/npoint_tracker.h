/*
 * npoint_tracker.h
 *
 *  Created on: May 26, 2014
 *      Author: raraujo
 */

#ifndef NPOINT_TRACKER_H_
#define NPOINT_TRACKER_H_
#include "npoint_packet.h"


#define DEFAULT_PERIOD		100

struct NPointArg{
	bool running;
	bool stopRequested;//Pointer to the stop requesting flag
	int sockedFd;//Socket file descriptor
	pthread_mutex_t * socketMutex;//shared mutex to protect the serial port.
	int period; //period of reporting in milliseconds
	uint64_t * lastTimestamp;
};


/**
 *
 * @param arg
 */
void * npointTracker(void *arg);


#endif /* NPOINT_TRACKER_H_ */
