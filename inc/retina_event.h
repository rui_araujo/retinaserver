/*
 * retina_event.h
 *
 *  Created on: May 21, 2014
 *      Author: raraujo
 */

#ifndef RETINA_EVENT_H_
#define RETINA_EVENT_H_

#include <stdint.h>

struct retinaEvent{
	uint8_t x;//X coordinate of the event
	uint8_t y;//Y coordinate of the event
	uint8_t polarity;//polarity of the event
	uint32_t timestamp;//(optional) time stamp of the event
};

// ***************************************************************************** data formats
#define EDVS_DATA_FORMAT_DEFAULT			EDVS_DATA_FORMAT_BIN

#define EDVS_DATA_FORMAT_BIN				 0					//  2 Bytes/event
#define EDVS_DATA_FORMAT_BIN_TS2B			 1					//  4 Bytes/event
#define EDVS_DATA_FORMAT_BIN_TS3B			 2					//  5 Bytes/event
#define EDVS_DATA_FORMAT_BIN_TS7B			 3					//  variable time stamp mode 3-5 Bytes/event

#define ADDR_X            	 (0x7F)// address bits X
#define ADDR_Y             	 (0x7F)// address bits Y
#define ADDR_P              (0x80)// bit specifying polarity of event


#define TIMESTAMP_7B        (0x7F)// bits specifying timestamp of event

/**
 * Helper function which is used to know whether or nor a packet should be sent through the socket, ie,
 * if there is still room left for another event.
 * @param eDVSDataFormat the retina event data format used
 * @return -1 if not recognized, otherwise the maximum data usage by a retina event
 */
int getMaximumEventSize(int eDVSDataFormat);

#endif /* RETINA_EVENT_H_ */
